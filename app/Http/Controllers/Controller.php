<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function index(){
        return view('partials/index', [
            'judul' => 'Home',
            'nama' => 'I Komang Marjaya Adi Surya',
            'tl' => '21, Maret 2001',
            'hp' => '+6281 156 847 361',
            'alamat' => 'Jl. Paleg, Desa Tianyar, Kec. Kubu, Karangasem',
            'mail' => 'marjaya@undiksha.ac.id',
            'git' => 'https://github.com/adisurya2019'
        ]);
    }
    public function about(){
        return view('partials/about', [
            'sname' => 'Adi Surya',
            'judul' => 'Tentang',
            'post' => 'Panggilan akrab Adi,
                        saat ini merupakan mahasiswa Universitas Pendidikan
                        Ganesha dan pernah mengenyam pendidikan di SMA
                        Negeri 1 Kubu jurusan IPA. Lahir di Tianyar, 21 Maret
                        2001. Adi merupakan anak terakhir dari tiga bersaudara,
                        saat ini tinggal di Desa Tianyar, Kecamatan Kubu,
                        Kabupaten Karangasem, Bali.'
        ]);
    }
    public function pendidikan(){
        return view('partials/pendidikan', [
            'judul' => 'Pendidikan',
            'sd' => 'SDN 6 Tianyar adalah Sekolah Dasar (SD) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Karang Asem dengan alamat Br. Dinas Darmawinangun - Kubu.',
            'smp' => 'SMPN 2 Kubu adalah Sekolah Menengah Pertama (SMP) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Karang Asem dengan alamat Jln. Pura Dalem No. 1 Tianyar.',
            'sma' => 'SMAN 1 Kubu adalah Sekolah Menengah Atas (SMA) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Karang Asem dengan alamat Desa Sukadana.'
        ]);
    }
    public function portofolio(){
        return view('partials/porto', [
            'judul' => 'Portofolio'
        ]);
    }
}
