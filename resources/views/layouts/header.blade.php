<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Adi Surya | {{$judul}} </title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
 <!-- Custom Theme files -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
	<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts -->

</head>
	<body>
		<!-- container -->
		<!-- header -->
        <div id="home" class="header">
            <div class="container">
            <!-- top-hedader -->
            <div class="top-header">
                <!-- /logo -->
                <!--top-nav---->
                <div class="top-nav">
                <div class="navigation">
                <div class="logo">
                    <h1><a href="/">Personal Profil</a></h1>
                </div>
                <div class="navigation-right">
                    <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <nav class="link-effect-3" id="link-effect-3">
                        <ul class="nav1 nav nav-wil">
                            <li class="scroll"><a data-hover="Home" href="/">Home</a></li>
                            <li><a class="scroll" data-hover="Tentang" href="/about">Tentang</a></li>
                            <li><a class="scroll" data-hover="Pendidikan" href="/pendidikan" >Pendidikan</a></li>
                            {{-- <li><a class="scroll" data-hover="Pengalaman" href="/pengalaman">Pengalaman</a></li> --}}
                            <li><a class="scroll" data-hover="Portfolio" href="/portofolio">Portfolio</a></li>
                        </ul>
                    </nav>
                        <!-- script-for-menu -->
                            <script>
                                $( "span.menu" ).click(function() {
                                $( "ul.nav1" ).slideToggle( 300, function() {
                                    Animation complete.
                                    });
                                });
                            </script>
                        <!-- /script-for-menu -->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
                @yield('konten')
                