@extends('layouts/header')

@section('konten')
<!--riwayat pendidikan-->
<div id="work" class="work">
    <div class="container">
        <div class="service-head text-center">
            <h3><span>RIWAYAT PENDIDIKAN</span></h3>
            <span class="border one"></span>
        </div>
        <div class="time-main w3l-agile">
            <div class="col-md-6 year-info">
                <ul class="year">
                    <li>2007 - 2013</li>
                    <li>2013 - 2016</li>
                    <li>2016 - 2019</li>
                    <li>2019 - SEKARANG</li>
                    <div class="clearfix"></div>
                </ul>
            </div> 
                <ul class="col-md-6 timeline">
                    <li>
                        <div class="timeline-badge info"><i class="glyphicon glyphicon-book"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">SD Negeri 6 Tianyar</h4>
                                </div>
                                <div class="timeline-body">
                                    <p>{{$sd}} </p>
                                </div>
                            </div>
                    </li>
                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-book"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">SMP Negeri 2 Kubu</h4>
                                </div>
                                <div class="timeline-body">
                                    <p>{{$smp}} </p>
                                </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge danger"><i class="glyphicon glyphicon-book"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">SMA Negeri 1 Kubu</h4>
                                </div>
                                <div class="timeline-body">
                                    <p>{{$sma}} </p>
                                </div>
                            </div>
                    </li>
                    <li>
                        <div class="timeline-badge success"><i class="glyphicon glyphicon-book"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">Universitas Pendiidkan Ganesha</h4>
                                </div>
                                <div class="timeline-body">
                                    <p>Universitas Pendidikan Ganesha adalah Perguruan Tinggi (PT) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Buleleng dengan alamat Jalan Ahmad Yani No 67 Singaraja.</p>
                                </div>
                        </div>
                    </li>
                </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

@endsection