@extends('layouts/header')

@section('konten')
    <!--work-experience-->
    <div id="work" class="work">
        <div class="container">
        <div class="service-head text-center">
                <h3><span>RIWAYAT PENDIDIKAN</span></h3>
                <span class="border one"></span>
            </div>
            <div class="time-main w3l-agile">
                    <div class="col-md-6 year-info">
                        <ul class="year">
                            <li>2014 - 2015</li>
                            <li>2017 - 2018</li>
                            <li>2016 - 2019</li>
                            <li>2018 - SEKARANG</li>
                            <div class="clearfix"></div>
                        </ul>
                    </div> 
                        <ul class="col-md-6 timeline">
                            <li>
                                <div class="timeline-badge info"><i class="glyphicon glyphicon-book"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Koor Sekbid TIK</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>OSIS SMPN 2 Kubu</p>
                                        </div>
                                </div>
                            </li>
                            <li>
                                <div class="timeline-badge primary"><i class="glyphicon glyphicon-book"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">SMP Negeri 2 Kubu</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>SMPN 2 Kubu adalah Sekolah Menengah Pertama (SMP) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Karang Asem dengan alamat Jln. Pura Dalem No. 1 Tianyar.</p>
                                        </div>
                                </div>
                            </li>
                            <li>
                                <div class="timeline-badge danger"><i class="glyphicon glyphicon-book"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">SMA Negeri 1 Kubu</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>SMAN 1 Kubu adalah Sekolah Menengah Atas (SMA) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Karang Asem dengan alamat Desa Sukadana.</p>
                                        </div>
                                    </div>
                            </li>
                            
                            <li>
                                <div class="timeline-badge success"><i class="glyphicon glyphicon-book"></i></div>
                                    <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">Universitas Pendiidkan Ganesha</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p>Universitas Pendidikan Ganesha adalah Perguruan Tinggi (PT) Negeri yang berlokasi di Propinsi Bali Kabupaten Kab. Buleleng dengan alamat Jalan Ahmad Yani No 67 Singaraja.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                </div>
        </div>
    <!--//work-experience-->

@endsection