@extends('layouts/header')

@section('konten')
				<!-- halaman home -->
				<div class="banner-info">
					<div class="col-md-7 header-right">
						<h6>Pengembang Aplikasi</h6>
						<ul class="address">
						
						<li>
								<ul class="address-text">
									<li><b>NAMA </b></li>
									<li>{{$nama}} </li>
								</ul>
							</li>
							<li>
								<ul class="address-text">
									<li><b>T.L </b></li>
									<li>{{$tl}} </li>
								</ul>
							</li>
							<li>
								<ul class="address-text">
									<li><b>Nomor HP</b></li>
									<li>{{$hp}}</li>
								</ul>
							</li>
							<li>
								<ul class="address-text">
									<li><b>ALAMAT </b></li>
									<li>{{$alamat}}</li>
								</ul>
							</li>
							<li>
								<ul class="address-text">
									<li><b>E-MAIL </b></li>
									<li><a href="mailto:marjaya@undiksha.ac.id"> {{$mail}}</a></li>
								</ul>
							</li>
							<li>
								<ul class="address-text">
									<li><b>GITHUB </b></li>
									<li><a href="https://github.com/adisurya2019">{{$git}}</a></li>
								</ul>
							</li>
							
						</ul>
					</div>
					<div class="col-md-5 header-left">
						<img src="images/me.jpg" alt="">
					</div>
					<div class="clearfix"> </div>
							
				  </div>
				</div>
			</div>
		</div>
		
@endsection


