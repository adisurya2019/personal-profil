<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class, 'index'] );

Route::get('/about', [Controller::class, 'about'] );

Route::get('/pendidikan', [Controller::class, 'pendidikan'] );

Route::get('/portofolio', [Controller::class, 'portofolio']);

// Route::get('/pengalaman', function () {
//     return view('pengalaman', [
//         'judul' => 'Pengalaman'
//     ]);
// });
